%Input x: the level of significance (0,1,2,3)
function y = compute_pi(x)
figure,hold on;
eps = 0.5*10^(-x-1);
n_max = 1000*10^(x);
pi_prev = 0;
pi_new = 3;
change = 3;

    
%Check if computed values are converging to the given significance level
while change > eps 
    inside_circle = 0;
    inside_square = 0;
    X = rand(n_max,1);
    Y = rand(n_max,1);
    %Check if points in quarter circle
    X_in = X(X.*X+Y.*Y<1);
    X_out = X(X.*X+Y.*Y>=1);
    Y_in = Y(X.*X+Y.*Y<1);
    Y_out = Y(X.*X+Y.*Y>=1);
    %Calculate pi using counts of inside quarter circle divided by sample
    %size
    inside_circle = inside_circle+length(X_in);
    inside_square = inside_square+length(X_in)+length(X_out);
    pi_prev = pi_new;
    pi_new = 4 * inside_circle / inside_square;
    change = abs(pi_prev - pi_new);
    n_max = n_max + 1000*10^(x);
end
%Make plot
scatter(X_in,Y_in,'o','MarkerFaceColor','r');
scatter(X_out,Y_out,'o','MarkerFaceColor','b');
title(strcat('Significance Level: ',string(x),',Computed Pi: ',string(pi_new),',Number of Points: ',string(n_max)));
y = pi_new;
end
