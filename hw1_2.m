%Problem 2

%Precision: 2 significant figures
epsilon = 0.0005;
%amount to be added to each iteration sample size
n_max = 100000;
steps = 0;
%initial values
pi_prev = 0;
pi_new = 3;
change = 1;
%Check if computed values are converging to the given significance level
while change > epsilon
    
    steps = steps + 1;
    %Begin simulation
    inside_circle = 0;
    inside_square = 0;
    %Create entire random array of simulated points of the sample size
    X = rand(n_max,1);
    Y = rand(n_max,1);
    X = rand(n_max,1);
    Y = rand(n_max,1);
    X_in = X(X.*X+Y.*Y<1);
    X_out = X(X.*X+Y.*Y>=1);
    Y_in = Y(X.*X+Y.*Y<1);
    Y_out = Y(X.*X+Y.*Y>=1);
    
    inside_circle = inside_circle+length(X_in);
    inside_square = inside_square+length(X_in)+length(X_out);
   %Calculate approximation of pi
    pi_prev = pi_new;
    pi_new = 4 * inside_circle / inside_square;
    change = abs(pi_prev - pi_new);
    %Each step increases number of points by 100000
    n_max = n_max + 100000;
end
fprintf('Number of steps needed to reach 2 digit significance: %d \n',steps);
fprintf('Corresponding number of points needed: %d \n',n_max);
fprintf('Computed Pi: %.4f \n\n',pi_new);
%Precision: 3 significant figure
epsilon = 0.00005;
n_max = 1000000;
steps = 0;
pi_prev = 0;
pi_new = 3;
change = 1;
%Check if computed values are converging to the given significance level
while change > epsilon
    
    steps = steps + 1;
    %Begin simulation
    inside_circle = 0;
    inside_square = 0;
    %Number of points to simulate
    X = rand(n_max,1);
    Y = rand(n_max,1);
    %Check if points in quarter circle
    X_in = X(X.*X+Y.*Y<1);
    X_out = X(X.*X+Y.*Y>=1);
    Y_in = Y(X.*X+Y.*Y<1);
    Y_out = Y(X.*X+Y.*Y>=1);
    
    inside_circle = inside_circle+length(X_in);
    inside_square = inside_square+length(X_in)+length(X_out);
   %Calculate approximation of pi
    pi_prev = pi_new;
    pi_new = 4 * inside_circle / inside_square;
    change = abs(pi_prev - pi_new);
    %Each step increases number of points by 1000000
    n_max = n_max + 1000000;
end
fprintf('Number of steps needed to reach 3 digit significance: %d \n',steps);
fprintf('Corresponding number of points needed: %d \n',n_max);
fprintf('Computed Pi: %.4f \n',pi_new);