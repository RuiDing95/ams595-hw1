Here is the user guide for the three MATLAB files I submitted for HW1.

In hw1_1.m, the code is used to produce the results for question 1. It iterates through 10,20,....,10000
as the number of points for Monte Carlo simulation of pi. For every sample size it records the computed pi value
along with the computation time. Running this code will produce two plots: A plot of the computed pi as it converges
to the real pi values and the error converging to zero; a second plot that shows the errors plotted against the computation
time for each sample size. Computation time stands for the running time of the sampling process.

In hw1_2.m, the code is used to produce the results for question 2. A while loop draws points from a sample size and 
examines if the level of convergence is enough for the level of precision needed. The sample size increases by 100000/1000000
per iteration for significance level 2/3. Running the code will print the results for significance level 2 and 3. It prints out the number of iterations and 
the final sample size needed as well as the estimated pi value.

In compute_pi.m, the code defines a function compute_pi to be used for computing pi to a user defined significance level.
The run the code, input compute_pi(x) in the command line where x is the integer (0,1,2,3) indicating the desired significance
level. The code follows a similar structure as question 2 and will return the computed pi value. A plot will be generated showing
all the points in the last iteration. The title includes the computed pi value, the significance level input, and the sample size used.

