%Problem 1
%Number of runs to perform
N = 1000;
%Initiate arrays for storage
Y = zeros(N);
X = zeros(N);
T = zeros(N);
for m = 1:N
    tic;
    %Begin simulation
    inside_circle = 0;
    inside_square = 0;
    %Number of points to simulate
    n_max = m*10;
    for n = 1:n_max
        x = rand;
        y = rand;
        %Check if random point in quarter circle
          if (x^2+y^2) <= 1
             
              inside_circle = inside_circle + 1;
              inside_square = inside_square + 1;
          elseif (x<=1 && y<=1 && x>=0 && y>=0)
            
              inside_square = inside_square + 1;
          else
           
          end
      
   %Calculate approximation of pi
    pi_computed = 4 * inside_circle / inside_square;
    Y(m) = pi-pi_computed;
    X(m) = n_max;
    end
    T(m) = toc;
end
%Making plots
figure
plot(X,Y)
hold on
plot(X,pi-Y)
title('Simulation Result')
xlabel('Number of Points')
[h,~] = legend('Error','Simulated Pi Value','Location','best');
  
figure 
plot(T,Y,'o')
title('Precision vs. Computational Cost')
xlabel('Computation Time')
ylabel('Error')
